## 1 Package Manager Information

- Package manager name: NPM
- Applicable Detectors: NPM Package Json Parse (least accurate)
- Primary technology or programming language: Nodejs (JavaScript)

### 1.1 Introduction Of Package Manager Tool

#### Public doc link

- NPM Home Page [https://www.npmjs.com/](https://www.npmjs.com/)
- Github repo [https://github.com/npm/documentation](https://github.com/npm/documentation)
- NPM version and nodejs version [https://docs.npmjs.com/cli/v10/commands/npm-version](https://docs.npmjs.com/cli/v10/commands/npm-version) and [https://nodejs.org/en/about/previous-releases](https://nodejs.org/en/about/previous-releases)
- NPM package.json related doc [https://medium.com/@hossam.hilal0/package-json-vs-package-lock-json-vs-npm-shrinkwrap-json-33fcddc1521a](https://medium.com/@hossam.hilal0/package-json-vs-package-lock-json-vs-npm-shrinkwrap-json-33fcddc1521a)

#### NPM support doc in detect user guide
[NPM Support](https://sig-product-docs.synopsys.com/bundle/integrations-detect/page/packagemgrs/npm.html)


#### Links to public samples: 

- Repo 1 `example1` [https://github.com/jfrog/project-examples.git](https://github.com/jfrog/project-examples.git)
```shell
git clone https://github.com/jfrog/project-examples.git
```
- Repo 2 (coming soon)

### 2. How to setup a demo env for NPM Package Json Parse

#### Prerequisites
docker is already installed in your env. Refer to [docker install guide](https://docs.docker.com/engine/install/)

#### 2.2.1 How to build up image
With below command
```shell
docker build -t npm-parse-detector:1.0 .
```

#### 2.2.2 startup container and run detector
* step 1: bring up container 
```shell
docker run -d --name npm-parse_demo npm-parse-detector:1.0
```
* step 2: enter container 
```shell
docker exec -it npm-parse_demo bash
```
* step 3: make the test scan \
Note: 1. Parts of detect properties are already set through environment variables in `Dockerfile`, the scan target path is fixed at `/srv/app` \ 2. because `NPM Package Json Parse` only parses a `package.json` file. it has least accurate, then `detect.accuracy.required` is required.
```shell
$ java -jar synopsys-detect-9.4.0.jar --blackduck.url=https://<url> --blackduck.trust.cert=true --detect.code.location.name=pipdockercli --blackduck.api.token=<token> --logging.level.com.synopsys.integration=DEBUG --detect.accuracy.required=NONE
```

#### 2.2.3 How to recycle containers (optional)
stop the container and remove it
```shell
docker stop npm-parse_demo ;docker rm npm-parse_demo 
```

### 3. Tips or Other information
coming soon
