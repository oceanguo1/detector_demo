## 1 Overview

C and CPP projects don't have a standard package manager or method for managing dependencies.  It is therefore more difficult to create an accurate BOM for these projects.  This leaves Software Composition Analysis tools fewer options than with other languages.  The primary options which are available in this context are: file system signatures.  Black Duck has a variety of old and new signatures which can be used to build a BOM.  In order to effectively use signatures, the tool first needs to know which files to take signatures from.  In the past SCA tools have pointed a scanner at a build directory, getting signatures from a subset of files within the directory sub-tree.  The problem with this approach is that there are many environmental variables, parameters and switches provided to the build tools, which make reference to files outside of the build directory to include as part of the build.  Further, there are, commonly, files within the build directory, which are not part of the build and can lead to false positives within the BOM. 

The new Black Duck C/CPP tool avoids the pitfalls described above by using a feature of Coverity called Build Capture.  Coverity Build Capture, wraps your build, observing all invocations of compilers and linkers and storing the paths of all compiled source code, included header files and linked object files.  These files are then matched using a variety of methods described in the section of this document called "The BOM".

### 1.1 Introduction Of Blackduck-c-cpp

#### Public doc link

- Tool Home Page [https://pypi.org/project/blackduck-c-cpp/](https://pypi.org/project/blackduck-c-cpp/)

#### Links to public samples: 

- Repo 1 `cpuminer`[https://github.com/pooler/cpuminer/tree/master](https://github.com/pooler/cpuminer/tree/master)
```shell
git clone https://github.com/pooler/cpuminer.git
```
- Repo 2 (coming soon)

### 2. How to setup a demo env for SBT detector

#### Prerequisites
docker is already installed in your env. Refer to [docker install guide](https://docs.docker.com/engine/install/)

#### 2.2.1 How to build up image
With below command
```shell
docker build -t blackduck-c-cpp-detector:1.0 .
```

#### 2.2.2 startup container and run detector
* step 1: bring up  and enter container
```shell
docker run -d --name blackduck-c-cpp_demo blackduck-c-cpp-detector:1.0 
docker exec -it blackduck-c-cpp_demo bash
```
* step 2: install blackduck-c-cpp and update the config.yaml file
```shell
pip install blackduck-c-cpp
vi /detect-download/config.yaml 
```
Only update: `bd_url` and `api_token`.

* step 3: make the test scan \
```shell
blackduck-c-cpp -c /detect-download/config.yaml 
```

### 3. Tips or Other information
Pending...
