## 1 Package Manager Information

- Package manager name: NuGet
- Applicable Detectors: NuGet Project Native Inspector
- Primary technology or programming language: .NET (C, C++, C#)

### 1.1 Introduction Of Package Manager Tool

#### Public doc link

- NuGet Home Page [https://www.nuget.org/](https://www.nuget.org/)

#### NuGet support doc in detect user guide
[NuGet Support](https://sig-product-docs.synopsys.com/bundle/integrations-detect/page/packagemgrs/nuget.html)


#### Links to public samples: 

- Repo 1 `example1` [https://github.com/fullstackhero/dotnet-webapi-boilerplate.git](https://github.com/fullstackhero/dotnet-webapi-boilerplate.git)
```shell
git clone -b 0.0.6-rc https://github.com/fullstackhero/dotnet-webapi-boilerplate.git
```
- Repo 2 (coming soon)

### 2. How to setup a demo env for NuGet Project Native detector

#### Prerequisites
docker is already installed in your env. Refer to [docker install guide](https://docs.docker.com/engine/install/)

#### 2.2.1 How to build up image
With below command
```shell
docker build -t nuget-project-native-detector:1.0 .
```

#### 2.2.2 startup container and run detector
* step 1: bring up container 
```shell
docker run -d --name nuget_project_native_demo nuget-project-native-detector:1.0
```
* step 2: enter container 
```shell
docker exec -it nuget_project_native_demo bash
```
* step 3: make the test scan \
Note: Parts of detect properties are already set through environment variables in `Dockerfile`, the scan target path is fixed at `/srv/app`
```shell
$ java -jar synopsys-detect-9.4.0.jar --blackduck.url=https://<url> --blackduck.trust.cert=true --detect.code.location.name=pipdockercli --blackduck.api.token=<token> --logging.level.com.synopsys.integration=DEBUG 
```

#### 2.2.3 How to recycle containers (optional)
stop the container and remove it
```shell
docker stop nuget_project_native_demo ;docker rm nuget_project_native_demo
```

### 3. Tips or Other information
Pending...
