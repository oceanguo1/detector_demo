## 1 Package Manager Information

- Package manager name: Lerna
- Applicable Detectors: Lerna detector
- Primary technology or programming language: JavaScript

### 1.1 Introduction Of Package Manager Tool

#### Public doc link

- Lerna Home Page [https://lerna.js.org/docs/introduction](https://lerna.js.org/docs/introduction)

#### Lerna support doc in detect user guide
[Lerna Support](https://sig-product-docs.synopsys.com/bundle/integrations-detect/page/packagemgrs/lerna.html)

#### Links to public samples: 

- Repo 1 `example1` [https://github.com/lerna/getting-started-example](https://github.com/lerna/getting-started-example)
```shell
git clone https://github.com/pnpm/sample-project.git
```
- Repo 2 (coming soon)

### 2. How to setup a demo env for Lerna detector

#### Prerequisites
docker is already installed in your env. Refer to [docker install guide](https://docs.docker.com/engine/install/)

#### 2.2.1 How to build up image
With below command
```shell
docker build -t lerna-detector:1.0 .
```

#### 2.2.2 startup container and run detector
* step 1: bring up container 
```shell
docker run -d --name lerna_demo lerna-detector:1.0
```
* step 2: enter container 
```shell
docker exec -it lerna_demo bash
```
* step 3: make the test scan \
Note: Parts of detect properties are already set through environment variables in `Dockerfile`, the scan target path is fixed at `/srv/app`
```shell
$ java -jar synopsys-detect-9.6.0.jar --blackduck.url=https://<url> --blackduck.trust.cert=true --detect.code.location.name=pipdockercli --blackduck.api.token=<token> --logging.level.com.synopsys.integration=DEBUG 
```

#### 2.2.3 How to recycle containers (optional)
stop the container and remove it
```shell
docker stop lerna_demo ;docker rm lerna_demo
```

### 3. Tips or Other information
Pending...
