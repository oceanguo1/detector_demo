## 1 Package Manager Information

- Package manager name: PNPM
- Applicable Detectors: PNPM detector
- Primary technology or programming language: JavaScript

### 1.1 Introduction Of Package Manager Tool

#### Public doc link

- PNPM Home Page [https://pnpm.io/](https://pnpm.io/)
- Github repo [https://github.com/pnpm/pnpm?tab=readme-ov-file](https://github.com/pnpm/pnpm?tab=readme-ov-file)
- PNPM lockfile spec [https://github.com/pnpm/spec/tree/master/lockfile](https://github.com/pnpm/spec/tree/master/lockfile)

#### PNPM support doc in detect user guide
[PNPM Support](https://sig-product-docs.synopsys.com/bundle/integrations-detect/page/packagemgrs/pnpm.html)


#### Links to public samples: 

- Repo 1 with `lockfile Version 5.3` [https://github.com/pnpm/sample-project.git](https://github.com/pnpm/sample-project.git)
```shell
git clone https://github.com/pnpm/sample-project.git
```
- Repo 2 with `lockfile Version 6` [https://github.com/byCedric/expo-monorepo-example.git](https://github.com/byCedric/expo-monorepo-example.git)
```shell
git clone https://github.com/byCedric/expo-monorepo-example.git
``` 
- Repo 3 (coming soon)

### 2. How to setup a demo env for PNPM detector

#### Prerequisites
docker is already installed in your env. Refer to [docker install guide](https://docs.docker.com/engine/install/)

#### 2.2.1 How to build up image
With below command
```shell
docker build -t pnpm-detector:1.0 .
```

#### 2.2.2 startup container and run detector
* step 1: bring up container 
```shell
docker run -d --name pnpm_demo pnpm-detector:1.0
```
* step 2: enter container 
```shell
docker exec -it pnpm_demo bash
```
* step 3: make the test scan \
Note: Parts of detect properties are already set through environment variables in `Dockerfile`, the scan target path is fixed at `/srv/app`
```shell
$ java -jar synopsys-detect-9.4.0.jar --blackduck.url=https://<url> --blackduck.trust.cert=true --detect.code.location.name=pipdockercli --blackduck.api.token=<token> --logging.level.com.synopsys.integration=DEBUG 
```

#### 2.2.3 How to recycle containers (optional)
stop the container and remove it
```shell
docker stop pnpm_demo ;docker rm pnpm_demo
```

### 3. Tips or Other information
Pending...
