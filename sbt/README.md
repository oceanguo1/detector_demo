## 1 Package Manager Information

- Package manager name: SBT
- Applicable Detectors: SBT detector
- Primary technology or programming language: SBT is built for Scala and Java projects

### 1.1 Introduction Of Package Manager Tool

#### Public doc link

- SBT Home Page [https://www.scala-sbt.org/](https://www.scala-sbt.org/)
- The repo in github [https://github.com/sbt/sbt](https://github.com/sbt/sbt)

#### SBT support doc in detect user guide
[SBT Support](https://sig-product-docs.synopsys.com/bundle/integrations-detect/page/packagemgrs/sbt.html)


#### Links to public samples: 

- Repo 1 `scala3-example-project`[https://github.com/scala/scala3-example-project](https://github.com/scala/scala3-example-project)
```shell
git clone https://github.com/scala/scala3-example-project
```
- Repo 2 (coming soon)

### 2. How to setup a demo env for SBT detector

#### Prerequisites
docker is already installed in your env. Refer to [docker install guide](https://docs.docker.com/engine/install/)

#### 2.2.1 How to build up image
With below command
```shell
docker build -t sbt-detector:1.0 .
```

#### 2.2.2 startup container and run detector
* step 1: bring up container 
```shell
docker run -d --name sbt_demo sbt-detector:1.0
```
* step 2: enter container 
```shell
docker exec -it sbt_demo bash
```
* step 3: make the test scan \
Note: Parts of detect properties are already set through environment variables in `Dockerfile`, the scan target path is fixed at `/srv/app`
```shell
$ java -jar synopsys-detect-9.4.0.jar --blackduck.url=https://<url> --blackduck.trust.cert=true --detect.code.location.name=<scan_name> --blackduck.api.token=<token> --logging.level.com.synopsys.integration=DEBUG 
```

#### 2.2.3 How to recycle containers (optional)
stop the container and remove it
```shell
docker stop sbt_demo ;docker rm sbt_demo
```

### 3. Tips or Other information
- Note: SBT 1.9.x and later versions are supported yet
- You can make the following adjustments to your build:
```shell
project/build.properties
sbt.version=1.10.1
```
- Pending...
