## 1 Package Manager Information

- Package manager name: Go Mod Cli
- Applicable Detectors: GO_MOD detector
- Primary technology or programming language: GoLang

### 1.1 Introduction Of Package Manager Tool

#### Public doc link

- Go Mod Home Page [https://go.dev/ref/mod](https://go.dev/ref/mod)

#### GoLang support doc in detect user guide
[GoLang Support](https://sig-product-docs.synopsys.com/bundle/integrations-detect/page/packagemgrs/golang.html)


#### Links to public samples: 

- Repo 1 `cobra`[https://github.com/spf13/cobra](https://github.com/spf13/cobra)
```shell
git clone https://github.com/spf13/cobra
```
- Repo 2 (coming soon)

### 2. How to setup a demo env for SBT detector

#### Prerequisites
docker is already installed in your env. Refer to [docker install guide](https://docs.docker.com/engine/install/)

#### 2.2.1 How to build up image
With below command
```shell
docker build -t gomod-detector:1.0 .
```

#### 2.2.2 startup container and run detector
* step 1: bring up container 
```shell
docker run -d --name gomod_demo gomod-detector:1.0
```
* step 2: enter container 
```shell
docker exec -it gomod_demo bash
```
* step 3: make the test scan \
Note: Parts of detect properties are already set through environment variables in `Dockerfile`, the scan target path is fixed at `/srv/app`
```shell
$ java -jar synopsys-detect-9.4.0.jar --blackduck.url=https://<url> --blackduck.trust.cert=true --detect.code.location.name=<scan_name> --blackduck.api.token=<token> --logging.level.com.synopsys.integration=DEBUG 
```

#### 2.2.3 How to recycle containers (optional)
stop the container and remove it
```shell
docker stop gomod_demo ;docker rm gomod_demo
```

### 3. Tips or Other information
Pending...
