## 1 Package Manager Information

- Package manager name: NPM
- Applicable Detectors: NPM CLI detector
- Primary technology or programming language: Nodejs (JavaScript)

### 1.1 Introduction Of Package Manager Tool

#### Public doc link

- NPM Home Page [https://www.npmjs.com/](https://www.npmjs.com/)
- Github repo [https://github.com/npm/documentation](https://github.com/npm/documentation)
- NPM version and nodejs version [https://docs.npmjs.com/cli/v10/commands/npm-version](https://docs.npmjs.com/cli/v10/commands/npm-version) and [https://nodejs.org/en/about/previous-releases](https://nodejs.org/en/about/previous-releases)
- NPM package.json related doc [https://medium.com/@hossam.hilal0/package-json-vs-package-lock-json-vs-npm-shrinkwrap-json-33fcddc1521a](https://medium.com/@hossam.hilal0/package-json-vs-package-lock-json-vs-npm-shrinkwrap-json-33fcddc1521a)

#### NPM support doc in detect user guide
[NPM Support](https://sig-product-docs.synopsys.com/bundle/integrations-detect/page/packagemgrs/npm.html)


#### Links to public samples: 

- Repo 1 `example1` [https://github.com/jfrog/project-examples.git](https://github.com/jfrog/project-examples.git)
```shell
git clone https://github.com/jfrog/project-examples.git
```
- Repo 2 (coming soon)

### 2. How to setup a demo env for NPM CLI detector

#### Prerequisites
docker is already installed in your env. Refer to [docker install guide](https://docs.docker.com/engine/install/)

#### 2.2.1 How to build up image
With below command
```shell
docker build -t npm-cli-detector:1.0 .
```

#### 2.2.2 startup container and run detector
* step 1: bring up container 
```shell
docker run -d --name npm-cli_demo npm-cli-detector:1.0
```
* step 2: enter container 
```shell
docker exec -it npm-cli_demo bash
```
* step 3: make the test scan \
Note: Parts of detect properties are already set through environment variables in `Dockerfile`, the scan target path is fixed at `/srv/app`
```shell
$ java -jar synopsys-detect-9.4.0.jar --blackduck.url=https://<url> --blackduck.trust.cert=true --detect.code.location.name=pipdockercli --blackduck.api.token=<token> --logging.level.com.synopsys.integration=DEBUG 
```

#### 2.2.3 How to recycle containers (optional)
stop the container and remove it
```shell
docker stop npm-cli_demo ;docker rm npm-cli_demo 
```

### 3. Tips or Other information

#### 3.1 Note:
* latest detector support node 20.5.1/npm 9.8.1
* start from detect9, support for npm is now extended to npm 9
* npm 6, which was deprecated in Synopsys Detect 8.x, is no longer supported, kindly use detect8.6 or previous version
* File format for "lockfileVersion" [https://docs.npmjs.com/cli/v9/configuring-npm/package-lock-json](https://docs.npmjs.com/cli/v9/configuring-npm/package-lock-json)
* No version provided: an "ancient" shrinkwrap file from a version of npm prior to npm v5.
* lockfileVersion 1: The lockfile version used by npm v5 and v6.
* lockfileVersion 2: The lockfile version used by npm v7 and v8. Backwards compatible to v1 lockfiles.
* lockfileVersion 3: The lockfile version used by npm v9. Backwards compatible to npm v7.

### 3.2 Compatibility env build up example
```shell
# step up npm6
docker build -f Dockerfile-npm6 -t npm-cli-detector:npm6.0 .
docker run -d --name npm6.0-cli_demo npm-cli-detector:npm6.0
docker exec -it npm6.0-pl_demo bash
```
