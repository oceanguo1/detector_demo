## 1 Package Manager Information

- Package manager name: Maven
- Applicable Detectors: Maven CLI detector
- Primary technology or programming language: Java

### 1.1 Introduction Of Package Manager Tool

#### Public doc link

- Maven Home Page [https://maven.apache.org/download.cgi](https://maven.apache.org/download.cgi)
- POM related doc  [https://maven.apache.org/guides/introduction/introduction-to-the-pom.html](https://maven.apache.org/guides/introduction/introduction-to-the-pom.html)

#### Maven support doc in detect user guide
[Maven Support](https://sig-product-docs.synopsys.com/bundle/integrations-detect/page/packagemgrs/maven.html)


#### Links to public samples: 

- Repo 1 `example1` [https://github.com/jfrog/project-examples.git](https://github.com/jfrog/project-examples.git)
```shell
git clone https://github.com/jfrog/project-examples.git
```
- Repo 2 `exapmle2` [https://github.com/jenkins-docs/simple-java-maven-app](https://github.com/jenkins-docs/simple-java-maven-app)
```shell
git clone https://github.com/jenkins-docs/simple-java-maven-app
``` 
- Repo 3 (coming soon)

### 2. How to setup a demo env for Maven CLI detector

#### Prerequisites
docker is already installed in your env. Refer to [docker install guide](https://docs.docker.com/engine/install/)

#### 2.2.1 How to build up image
With below command
```shell
docker build -t mvn-cli-detector:1.0 .
```

#### 2.2.2 startup container and run detector
* step 1: bring up container 
```shell
docker run -d --name mvn-cli_demo mvn-cli-detector:1.0
```
* step 2: enter container 
```shell
docker exec -it mvn-cli_demo bash
```
* step 3: make the test scan \
Note: Parts of detect properties are already set through environment variables in `Dockerfile`, the scan target path is fixed at `/srv/app`
```shell
$ java -jar synopsys-detect-9.4.0.jar --blackduck.url=https://<url> --blackduck.trust.cert=true --detect.code.location.name=pipdockercli --blackduck.api.token=<token> --logging.level.com.synopsys.integration=DEBUG 
```

#### 2.2.3 How to recycle containers (optional)
stop the container and remove it
```shell
docker stop mvn-cli_demo ;docker rm mvn-cli_demo
```

### 3. Tips or Other information
Pending...
