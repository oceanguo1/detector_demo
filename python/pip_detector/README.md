## 1 Package Manager Information

- Package manager name: PIP
- Applicable Detectors: PIP detectors
- Primary technology or programming language: Python

### 1.1 Introduction Of Package Manager Tool

#### Public doc link

- PIP User Guide [https://pip.pypa.io/en/stable/user_guide/](https://pip.pypa.io/en/stable/user_guide/)
- The Repo in github [https://github.com/pypa/pip](https://github.com/pypa/pip)

#### Python support doc in detect user guide
[Python Support](https://sig-product-docs.synopsys.com/bundle/integrations-detect/page/packagemgrs/python.html)


#### Links to public samples: 

- Repo 1 [https://github.com/blackducksoftware/hub-rest-api-python.git](https://github.com/blackducksoftware/hub-rest-api-python.git)
```shell
git clone https://github.com/blackducksoftware/hub-rest-api-python.git
```
- Repo 2 (coming soon)

### 2. How to setup a demo env for PIP detectors

#### Prerequisites
docker is already installed in your env. Refer to [docker install guide](https://docs.docker.com/engine/install/)

#### 2.2.1 How to build up image
With below command
```shell
docker build -t pip-detector:1.0 .
```

#### 2.2.2 startup container and run detector
* step 1: bring up container 
```shell
docker run -d --name pip_demo pip-detector:1.0
```
* step 2: enter container 
```shell
docker exec -it pip_demo bash
```
* step 3: make the test scan \
Note: Parts of detect properties are already set through environment variables in `Dockerfile`, the scan target path is fixed at `/srv/app`
```shell
$ java -jar synopsys-detect-9.4.0.jar --blackduck.url=https://<url> --blackduck.trust.cert=true --detect.code.location.name=pipdockercli --blackduck.api.token=<token> --logging.level.com.synopsys.integration=DEBUG 
```

#### 2.2.3 How to recycle containers (optional)
stop the container and remove it
```shell
docker stop pip_demo ;docker rm pip_demo
```

### 3. Tips or Other information
Pending...
