## 1 Package Manager Information

- Package manager name: Poetry
- Applicable Detectors: Poetry detector
- Primary technology or programming language: Python

### 1.1 Introduction Of Package Manager Tool

#### Public doc link

- Poetry Home Page [https://python-poetry.org/](https://python-poetry.org/)
- The repo in github [https://github.com/python-poetry/poetry.git](https://github.com/python-poetry/poetry.git)
```shell
wget https://github.com/python-poetry/poetry/archive/refs/tags/1.8.2.zip
```

#### Python support doc in detect user guide
[Python Support](https://sig-product-docs.synopsys.com/bundle/integrations-detect/page/packagemgrs/python.html)


#### Links to public samples: 

- Repo 1 `cookiecutter-poetry`[https://github.com/fpgmaas/cookiecutter-poetry](https://github.com/fpgmaas/cookiecutter-poetry)
```shell
git clone https://github.com/fpgmaas/cookiecutter-poetry
```
- Repo 2 (coming soon)

### 2. How to setup a demo env for Poetry detector

#### Prerequisites
docker is already installed in your env. Refer to [docker install guide](https://docs.docker.com/engine/install/)

#### 2.2.1 How to build up image
With below command
```shell
docker build -t poetry-detector:1.0 .
```

#### 2.2.2 startup container and run detector
* step 1: bring up container 
```shell
docker run -d --name poetry_demo poetry-detector:1.0
```
* step 2: enter container 
```shell
docker exec -it poetry_demo bash
```
* step 3: make the test scan \
Note: Parts of detect properties are already set through environment variables in `Dockerfile`, the scan target path is fixed at `/srv/app`
```shell
$ java -jar synopsys-detect-9.4.0.jar --blackduck.url=https://<url> --blackduck.trust.cert=true --detect.code.location.name=<scan_name> --blackduck.api.token=<token> --logging.level.com.synopsys.integration=DEBUG 
```

#### 2.2.3 How to recycle containers (optional)
stop the container and remove it
```shell
docker stop poetry_demo ;docker rm poetry_demo
```

### 3. Tips or Other information
Pending...
