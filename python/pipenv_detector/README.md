## 1 Package Manager Information

- Package manager name: Pipenv
- Applicable Detectors: PIPENV detectors (Pipenv lock detector, Pipfile lock detector)
- Primary technology or programming language: Python

### 1.1 Introduction Of Package Manager Tool

#### Public doc link

- Pipenv Home Page [https://pipenv.pypa.io/en/latest/](https://pipenv.pypa.io/en/latest/)
- The repo in github [https://github.com/pypa/pipenv.git](https://github.com/pypa/pipenv.git)


#### Python support doc in detect user guide
[Python Support](https://sig-product-docs.synopsys.com/bundle/integrations-detect/page/packagemgrs/python.html)


#### Links to public samples: 

- Repo 1 `pipenv`[https://github.com/pypa/pipenv](https://github.com/pypa/pipenv)
```shell
git clone https://github.com/pypa/pipenv
```
- Repo 2 (coming soon)

### 2. How to setup a demo env for Pipenv detector

#### Prerequisites
docker is already installed in your env. Refer to [docker install guide](https://docs.docker.com/engine/install/)

#### 2.2.1 How to build up image
With below command
```shell
docker build -t pipenv-detector:1.0 .
```

#### 2.2.2 startup container and run detector
* step 1: bring up container 
```shell
docker run -d --name pipenv_demo pipenv-detector:1.0
```
* step 2: enter container 
```shell
docker exec -it pipenv_demo bash
```
* step 3: make the test scan \
Note: Parts of detect properties are already set through environment variables in `Dockerfile`, the scan target path is fixed at `/srv/app`
```shell
$ java -jar synopsys-detect-9.4.0.jar --blackduck.url=https://<url> --blackduck.trust.cert=true --detect.code.location.name=<scan_name> --blackduck.api.token=<token> --logging.level.com.synopsys.integration=DEBUG 
```

#### 2.2.3 How to recycle containers (optional)
stop the container and remove it
```shell
docker stop pipenv_demo ;docker rm pipenv_demo
```

### 3. Tips or Other information
Pending...
